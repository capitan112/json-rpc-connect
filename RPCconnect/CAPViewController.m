//
//  CAPViewController.m
//  RPCconnect
//
//  Created by Капитан on 27.08.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "CAPViewController.h"
#import "CAPRPCConnection.h"

static const NSString *companyLogin    = @"develop";
static const NSString *keyAPI         = @"7f70f455304274c550704446e0c8d772655b14236f5d3c360168bcfbb06ce221";
static const NSString *urlToken        = @"http://user-api.ru.simplybook.me/login";
static const NSString *urlRegistration = @"https://user-api.simplybook.me/";

@interface CAPViewController ()

@end

@implementation CAPViewController

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _defaults = [NSUserDefaults standardUserDefaults];
        _receivedData = [[NSMutableData alloc] init];
        _token = [_defaults objectForKey:@"token"];
        NSLog(@"_token: %@", _token);
        if (!_token) {
            [self getToken];
        }
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    NSDictionary *getEventList = @{@"method" : @"getEventList"};
//    [self createRequestWithDict:getEventList];
    
//    NSDictionary *getUnitList = @{@"method" : @"getUnitList"};
//    [self createRequestWithDict:getUnitList];
    
//    NSDictionary *calculateEndTime = @{
//                                      @"method" : @"calculateEndTime",
//                                      @"params" : @[@"2014-09-01", @(1), @(1)]
//                                      };
//
//    [self createRequestWithDict:calculateEndTime];
    
    
   
//    NSDictionary *getTimeframe = @{@"method" : @"getTimeframe"};
//    [self createRequestWithDict:getTimeframe];
//
    
//    NSDictionary *getReservedTime = @{
//                                          @"method" : @"getReservedTime",
//                                          @"params" : @[@"2014-09-01", @"2014-09-01", @(1), @(1)]
//                                      };
//    [self createRequestWithDict:getReservedTime];
    
//    NSDictionary *getStartTimeMatrix = @{
//                                         @"method" : @"getStartTimeMatrix",
//                                         @"params" : @[@"2014-09-01", @"2014-09-01", @(1), @(1) ]
//                                         };
//    NSDictionary *startTime = [self createRequestWithDict:getStartTimeMatrix];
//    NSLog(@"startTime %@", startTime);

    
//    NSDictionary *getWorkDaysInfo  = @{
//                                       @"method" : @"getWorkDaysInfo",
//                                       @"params" : @[@"2014-08-29", @"2014-09-01", @(1)]
//                                       };
//    [self createRequestWithDict:getWorkDaysInfo];
    

//Client bookinkg
    NSDictionary *book  = @{
                                       @"method" : @"book",
                                       @"params" : @[@(1), @(1), @"2014-09-01", @"14:00:00", @{@"name": @"Dad", @"email": @"plrs@mail.ru", @"phone": @"+380504191155"}, @"true" ]
                                       };
    [self createRequestWithDict:book];

    
//Confirm bookinkg
//    NSDictionary *confirmBooking   = @{
//                                     @"method" : @"confirmBooking",
//                                     @"params" : @[@"uos4v4o"]
//                                     };
//    [self createRequestWithDict:confirmBooking ];

    
//Cancel bookinkg
//    NSDictionary *cancelBooking  = @{
//                                       @"method" : @"cancelBooking",
//                                       @"params" : @[@(1), @"uos4v4o"]
//                                       };
//    [self createRequestWithDict:cancelBooking];

//    NSDictionary *getFirstWorkingDay  = @{
//                                          @"method" : @"getFirstWorkingDay",
//                                          @"params" : @[@(1)]
//                                       };
//    [self createRequestWithDict:getFirstWorkingDay];
//   
//    NSString *requiredDay = @"2014-09-01";
//    NSDictionary *getStartTimeMatrix = @{
//                                         @"method" : @"getStartTimeMatrix",
//                                         @"params" : @[requiredDay, requiredDay, @(1), @(1) ]
//                                         };
//    NSDictionary *startTimeTemp = [self createRequestWithDict:getStartTimeMatrix];
//    NSArray *startTimeData = [[NSArray alloc]initWithArray:startTimeTemp[@"result"][requiredDay]];
//    NSMutableDictionary *shedduleTime = [[NSMutableDictionary alloc] init];
//    for (NSString *time in startTimeData) {
//        NSString *truncatedString = [time substringToIndex:[time length] - 3];
//        [shedduleTime setValue:@"available" forKey:truncatedString];
//    }
//    NSDictionary *getReservedTime = @{
//                                      @"method" : @"getReservedTime",
//                                      @"params" : @[requiredDay, requiredDay, @(1), @(1)]
//                                      };
//    NSDictionary *reservedTimeTemp = [self createRequestWithDict:getReservedTime];
//    NSArray *reservedTimeData = [[NSArray alloc]initWithArray:reservedTimeTemp[@"result"][requiredDay]];
////    NSLog(@"reservedTimeTemp %@", reservedTimeData[0][@"events"][0][@"from"]);
//    for (NSDictionary *busyTime in reservedTimeData) {
//        for(NSDictionary *busyHours in busyTime[@"events"]) {
//            [shedduleTime setValue:@"busy" forKey:busyHours[@"from"]];
//        }
//    }
//
//    NSArray* keys = [shedduleTime allKeys];
//    NSArray* sortedArray = [keys sortedArrayUsingComparator:^(id a, id b) {
//        return [a compare:b options:NSNumericSearch];
//    }];
    
//    for( NSString* aStr in sortedArray ) {
//        NSLog( @"%@ has key %@", [shedduleTime objectForKey:aStr], aStr );
//    }
    
//    NSArray* sortedKeys = [shedduleTime keysSortedByValueUsingSelector:@selector(comparator)];
//    NSLog(@"shedduleTime %@", sortedArray);
}



#pragma mark - requests

- (void)getToken {
    NSInteger idRequest = arc4random();
    NSDictionary *rpcDict = @{
                              @"jsonrpc" : @"2.0",
                              @"id" : @(idRequest),
                              @"method" : @"getToken",
                              @"params" : @[companyLogin, keyAPI]
                              };
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:rpcDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSDictionary *defaultHeader = @{
                                    @"Accept" : @"application/json",
                                    @"Content-Type": @"application/json"
                                    };
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[urlToken copy]]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:10];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:defaultHeader];
    [request setHTTPBody:jsonData];
     NSDictionary *serverDict = [self getDictFromServer:request];
    _token = serverDict[@"result"];
    [_defaults setObject:_token forKey:@"token"];
    [_defaults synchronize];
    NSLog(@"token setted");
}

- (NSDictionary *)createRequestWithDict:(NSDictionary *)dict {
    NSInteger idRequest = arc4random();
    NSDictionary *dictWithParamets = @{
                                       @"jsonrpc" : @"2.0",
                                       @"id" : @(idRequest),
                                       };
    NSMutableDictionary *rpcDict = [NSMutableDictionary dictionaryWithDictionary:dictWithParamets];
    [rpcDict addEntriesFromDictionary:dict];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:rpcDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSDictionary *defaultHeader = @{
                                    @"Accept" : @"application/json",
                                    @"Content-Type": @"application/json",
                                    @"X-Company-Login" : companyLogin,
                                    @"X-Token" : _token
                                   };
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]
                                    initWithURL:[NSURL URLWithString:[urlRegistration copy]]
                                    cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                    timeoutInterval:5];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:defaultHeader];
    [request setHTTPBody:jsonData];
    
    return [self getDictFromServer:request];
}


#pragma mark - getData from Server

- (NSDictionary *)getDictFromServer:(NSURLRequest *)request {
    NSURLResponse *response = nil;
    NSData *serverData = [NSURLConnection sendSynchronousRequest:request
                                               returningResponse:&response
                                                           error:nil];
    if (!serverData) {
        NSLog(@"Error getinng data form server");
    }
    
    NSError *dictionaryParceError;
    NSDictionary *serverDictionary = [NSJSONSerialization JSONObjectWithData:serverData
                                                                     options:NSJSONReadingMutableContainers
                                                                       error:&dictionaryParceError];
    NSLog(@"server data %@ %@", serverDictionary, dictionaryParceError);
    return serverDictionary;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
