//
//  CAPRPCConnection.m
//  RPCconnect
//
//  Created by Капитан on 27.08.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "CAPRPCConnection.h"


@interface CAPRPCConnection()

@property (nonatomic, strong) NSMutableDictionary *rpcDict;

@end


@implementation CAPRPCConnection

- (id)initWithLogin:(NSString *)login keyAPI:(NSString *)keyAPI {
    if (self = [super init]) {
        
        _rpcDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                        @"2.0", @"jsonrpc",
                                        @"getToken", @"method",
                                        @ {@"$companyLogin" : login, @"$apiKey" : keyAPI}, @"params",
                                        @"CapiExplorer-123", @"id",
                                        nil];
    }
    
    return self;
}

- (void) createRequest {
//    NSError *error;
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:_rpcDict
//                                                       options:NSJSONWritingPrettyPrinted
//                                                         error:&error];
//    NSString *urlString = [NSString stringWithFormat:@"http://user-api.ru.simplybook.me/login"];
//    _url = [NSURL URLWithString:urlString];

}



@end
