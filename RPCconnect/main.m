//
//  main.m
//  RPCconnect
//
//  Created by Капитан on 27.08.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CAPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CAPAppDelegate class]));
    }
}
