//
//  CAPViewController.h
//  RPCconnect
//
//  Created by Капитан on 27.08.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CAPViewController : UIViewController

@property (nonatomic, copy) NSString *token;
@property (nonatomic, strong) NSMutableData *receivedData;
@property (nonatomic, strong) NSUserDefaults *defaults;

@end
