//
//  CAPRPCConnection.h
//  RPCconnect
//
//  Created by Капитан on 27.08.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CAPRPCConnection : NSObject

@property (nonatomic, strong) NSURL *url;

- (id)initWithLogin:(NSString *)login keyAPI:(NSString *)keyAPI;

@end
